package main

import (
	"fmt"
	"os"

	"gitlab.com/destrealm/go/gab-crawler/cli/handlers"
	"github.com/urfave/cli"
)

// Version of the application.
var Version = "0.1.0"

// CommitHash injected during `go build`.
var CommitHash string

var Crawl = cli.Command{
	Name:  "crawl",
	Usage: "starts the crawler process",
	Subcommands: []cli.Command{
		{
			Name:   "groups",
			Usage:  "crawls Gab's groups",
			Action: handlers.CrawlGroups,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "credentials, l",
					Usage: "Login credentials file (YAML format).",
					Value: "credentials.yaml",
				},
				cli.StringFlag{
					Name:  "export, e",
					Usage: "Export collected group data as `TYPE`. \"json\" and \"csv\" are currently the only acceptable formats.",
					Value: "json",
				},
				cli.IntFlag{
					Name:  "max-errors, m",
					Usage: "Maximum errors before stopping the crawl.",
				},
				cli.BoolFlag{
					Name:  "no-save, n",
					Usage: "If specified, do not save any application state.",
				},
				cli.StringFlag{
					Name:  "output, o",
					Usage: "Output file. If this file exists, it will be renamed to <output>-YYMMDDHHMMSS.<ext>.",
					Value: "output",
				},
				cli.BoolFlag{
					Name:  "prompt, p",
					Usage: "Prompts for authentication credentials. Has no effect if -l is specified, \"credentials.yaml\" exists, or if prior state has been saved. If -t is specified, this will ask for the authorization token instead.",
				},
				cli.StringFlag{
					Name:  "state, S",
					Usage: "State file location for persisting collected authentication token.",
					Value: ".state.yaml",
				},
				cli.BoolFlag{
					Name:  "token, t",
					Usage: "Prompt for authorization token instead of the email address and password.",
				},
			},
		},
	},
}

func main() {

	app := cli.NewApp()
	app.Name = "Gab Crawler"
	app.Version = fmt.Sprintf("%s-%s", Version, CommitHash)

	app.Commands = []cli.Command{
		Crawl,
	}

	app.Run(os.Args)
}
