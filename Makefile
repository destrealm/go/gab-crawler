TEST=.

all: test build build-win32 build-macos

build:
	go build ./cmd/gabcrawler

build-macos:
	GOOS=darwin go build -o gabcrawler.darwin ./cmd/gabcrawler

build-win32:
	GOOS=windows go build ./cmd/gabcrawler

test:
	go test -run=$(TEST) ./...
