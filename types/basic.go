package types

import (
	"strconv"
	"time"

	"gitlab.com/destrealm/go/errors"
)

type flexInt int

func (f *flexInt) UnmarshalJSON(b []byte) error {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}

	i, err := strconv.Atoi(string(b))
	if err != nil {
		return err
	}
	*f = flexInt(i)

	return nil
}

type Group struct {
	ID            flexInt `json:"id"`
	Title         string  `json:"title"`
	Description   string  `json:"description"`
	CoverImageURL string  `json:"cover_image_url"`
	Archived      bool    `json:"is_archived"`
	MemberCount   int     `json:"member_count"`

	Posts []*GroupPost `json:"posts"`
}

type CrawlResult struct {
	Result interface{}
	Error  errors.ErrorTracer
}

type GroupPost struct {
	ID      flexInt   `json:"id"`
	Created time.Time `json:"created_at"`
}
