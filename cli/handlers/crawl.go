package handlers

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/destrealm/go/gab-crawler/api"
	"gitlab.com/destrealm/go/gab-crawler/app"
	ct "gitlab.com/destrealm/go/gab-crawler/types"
)

func CrawlGroups(ctx *cli.Context) error {
	// TODO: Load authentication information from state.

	start := 0
	stop := 0
	maxErrors := ct.MaxErrorsOnFetch

	config := &api.Configuration{
		Login:           &api.Login{},
		CredentialsFile: ctx.String("credentials"),
		ExportFormat:    strings.ToLower(ctx.String("export")),
		DoNotSaveState:  ctx.Bool("no-save"),
		OutputFile:      ctx.String("output"),
		StateFile:       ctx.String("state"),
	}

	if ctx.Bool("prompt") {
		if ctx.Bool("token") {
			config.Prompter = func() (*api.Login, error) {
				return FetchCredentials(true)
			}
		} else {
			config.Prompter = func() (*api.Login, error) {
				return FetchCredentials(false)
			}
		}
	}

	if ctx.NArg() >= 1 {
		v, err := strconv.Atoi(ctx.Args().Get(0))
		if err == nil {
			start = v
		}
	}

	if ctx.NArg() >= 2 {
		v, err := strconv.Atoi(ctx.Args().Get(1))
		if err == nil {
			stop = v
		}
	}

	maxErrors = ctx.Int("max-errors")

	crawler, err := app.NewCrawler(config)
	if err != nil {
		fmt.Println("failed to initialize crawler:", err)
		return err
	}
	_ = crawler
	err = crawler.Groups(&app.CrawlOptions{
		Start:     start,
		Stop:      stop,
		MaxErrors: maxErrors,
	})
	return err
}
