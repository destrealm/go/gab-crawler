package handlers

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"syscall"

	"gitlab.com/destrealm/go/gab-crawler/api"
	"golang.org/x/crypto/ssh/terminal"
)

// FetchCredentials collects and returns user credentials retrieved via
// prompting at the terminal. `email`, `password`, and `token` are defined as
// variables at the head of this function for auditing and to validate that they
// are not being set elsewhere as globals.
//
// Errors returned are unmanaged.
func FetchCredentials(asktoken bool) (*api.Login, error) {
	var email string
	var token string
	var password []byte
	var err error
	var login = &api.Login{}

	reader := bufio.NewReader(os.Stdin)
	if !asktoken {
		fmt.Print("Email: ")
		email, err = reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		login.Email = email

		fmt.Print("Password: ")
		password, err = terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return nil, err
		}
		login.Password = string(password)

		// Be polite.
		fmt.Println("")

		email = strings.TrimSuffix(email, "\n")
		email = strings.TrimSuffix(email, "\r") // Windows...

		return login, nil
	}

	fmt.Println("Token: ")
	token, err = reader.ReadString('\n')
	if err != nil {
		return nil, err
	}
	login.Token = token

	return login, nil
}
