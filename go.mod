module gitlab.com/destrealm/go/gab-crawler

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/urfave/cli v1.20.0
	gitlab.com/destrealm/go/errors v0.0.0-20190929053902-9518f229b9b9
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	gopkg.in/yaml.v2 v2.2.2
)
