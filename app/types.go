package app

import (
	"io/ioutil"
	"os"

	. "gitlab.com/destrealm/go/gab-crawler/errors"
	"gopkg.in/yaml.v2"
)

// CredentialsFile defines the layout of the YAML configuration in which the
// user's authentication is expected to be found. If this file is empty or
// doesn't exist, the user will be prompted for their credentials.
//
// The credentials file isn't necessary but may be useful for automating the
// client via a token.
type CredentialsFile struct {
	AuthorizationToken string `yaml:"authorization_token"`
	EmailAddress       string `yaml:"email_address"`
	Password           string `yaml:"password"`
}

// CredentialsFileFromPath returns a CredentialsFile as returned from the
// specified path, or an empty CredentialsFile struct if it does not exist.
func CredentialsFileFromPath(path string) *CredentialsFile {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return &CredentialsFile{}
	}

	var credentials *CredentialsFile

	b, err := ioutil.ReadFile(path)
	if err != nil {
		return &CredentialsFile{}
	}

	if err := yaml.Unmarshal(b, credentials); err != nil {
		return &CredentialsFile{}
	}

	return credentials
}

type CrawlerState struct {
	Token     string `yaml:"token"`
	LastCrawl string `yaml:"last_crawl"`
	LastID    int    `yaml:"last_id"`
	path      string
}

func CrawlerStateFromPath(path string) (*CrawlerState, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, ErrInvalidStateFile.Do(err)
	}

	state := &CrawlerState{}

	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, ErrInvalidStateFile.Do(err)
	}

	if err := yaml.Unmarshal(b, state); err != nil {
		return nil, ErrInvalidStateFile.Do(err)
	}

	state.path = path
	return state, nil
}

func (c *CrawlerState) Save() error {
	b, err := yaml.Marshal(c)
	if err != nil {
		return ErrSavingState.Do(err)
	}

	if c.path == "" {
		c.path = ".state.yaml"
	}

	if err := ioutil.WriteFile(c.path, b, os.FileMode(0600)); err != nil {
		return ErrSavingState.Do(err)
	}

	return nil
}

type CrawlOptions struct {
	Start     int
	Stop      int
	MaxErrors int
}
