package app

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"strconv"
	"strings"
	"time"

	. "gitlab.com/destrealm/go/gab-crawler/errors"
	ct "gitlab.com/destrealm/go/gab-crawler/types"
)

type GroupWriter interface {
	Close() error
	Write(*ct.Group) error
	WriteAll([]*ct.Group) error
}

// NewGroupWriter creates a new serializer for group data based on the specified
// format. If an invalid format is requested, ErrInvalidFormatRequested will be
// returned as the error.
func NewGroupWriter(output io.WriteCloser, format string) (GroupWriter, error) {
	format = strings.ToLower(format)

	if format == "json" {
		return &GroupJSONWriter{File: output}, nil
	} else if format == "csv" {
		return &GroupCSVWriter{File: output, CSV: csv.NewWriter(output)}, nil
	}

	return nil, ErrInvalidFormatRequested
}

type GroupJSONWriter struct {
	File io.WriteCloser
	open bool
}

// Close the JSON output and file interface.
//
// Does not wrap errors.
func (g *GroupJSONWriter) Close() error {
	if !g.open {
		return nil
	}
	g.open = false
	if _, err := g.File.Write([]byte("]")); err != nil {
		return err
	}
	return g.File.Close()
}

// Write a group as JSON-formatted data to the file writer.
//
// Does not wrap errors.
func (g *GroupJSONWriter) Write(group *ct.Group) error {
	if !g.open {
		if _, err := g.File.Write([]byte("[")); err != nil {
			return err
		}
	}

	buf, err := json.Marshal(group)
	if err != nil {
		return err
	}

	g.File.Write(buf)
	return nil
}

// WriteAll groups as JSON-formatted data to the file writer.
//
// Does not wrap errors.
func (g *GroupJSONWriter) WriteAll(groups []*ct.Group) error {
	for _, group := range groups {
		if err := g.Write(group); err != nil {
			return err
		}
	}
	return nil
}

type GroupCSVWriter struct {
	File   io.WriteCloser
	CSV    *csv.Writer
	header bool
}

// Close the CSV output and file interface.
//
// Does not wrap errors.
func (g *GroupCSVWriter) Close() error {
	g.CSV.Flush()
	return g.File.Close()
}

// Write a group as CSV-formatted data to the file writer.
//
// Does not wrap errors.
func (g *GroupCSVWriter) Write(group *ct.Group) error {
	if !g.header {
		err := g.CSV.Write([]string{
			"id",
			"title",
			"description",
			"cover_image_url",
			"is_archive",
			"member_count",
			"most_recent_post",
		})
		if err != nil {
			return err
		}
		g.header = true
	}

	record := make([]string, 7)
	record[0] = strconv.Itoa(int(group.ID))
	record[1] = group.Title
	record[2] = group.Description
	record[3] = group.CoverImageURL
	record[4] = "false"
	record[5] = strconv.Itoa(group.MemberCount)
	record[6] = ""

	if group.Archived {
		record[4] = "true"
	}

	if len(group.Posts) > 0 {
		record[6] = group.Posts[0].Created.Format(time.RFC3339)
	}

	if err := g.CSV.Write(record); err != nil {
		return err
	}

	g.CSV.Flush()
	return nil
}

// WriteAll groups as CSV-formatted data to the file writer.
//
// Does not wrap errors.
func (g *GroupCSVWriter) WriteAll(groups []*ct.Group) error {
	for _, group := range groups {
		if err := g.Write(group); err != nil {
			return err
		}
	}
	return nil
}
