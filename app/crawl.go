package app

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"

	_ "gitlab.com/destrealm/go/errors"
	"gitlab.com/destrealm/go/gab-crawler/api"
	ct "gitlab.com/destrealm/go/gab-crawler/types"
)

// Crawler is a high-level representation of the Gab client and controls access
// to resources, managing some state. In particular, responses returned by the
// client will be written by the Crawler to permanent storage or elsewhere
// (depending on configuration).
type Crawler struct {
	configuration *api.Configuration
}

// NewCrawler configures and returns a Crawler instance.
func NewCrawler(configuration *api.Configuration) (*Crawler, error) {
	crawler := &Crawler{
		configuration: configuration,
	}
	if err := crawler.Init(); err != nil {
		return nil, err
	}

	return crawler, nil
}

func (c *Crawler) Init() error {
	state, err := CrawlerStateFromPath(c.configuration.StateFile)
	if err == nil && state.Token != "" {
		c.configuration.Login.Token = state.Token
	}

	if c.configuration.Login.IsEmpty() && c.configuration.Prompter != nil {
		login, err := c.configuration.Prompter()
		// TODO: Logging.
		if err != nil {
			fmt.Println("prompting user for authentication details failed")
			fmt.Println("this should not normally happen; file a bug")
		}

		c.configuration.Login = login
	}

	if c.configuration.Login.Token != "" {
		return nil
	}

	if err := c.TestLogin(); err != nil {
		fmt.Println("login failed:", err)
		return err
	} else {
		state := &CrawlerState{
			Token: c.configuration.Login.Token,
			path:  c.configuration.StateFile,
		}

		if !c.configuration.DoNotSaveState {
			if err := state.Save(); err != nil {
				fmt.Println("error saving authentication token:", err)
				return err
			}
		}
	}

	return nil
}

// Login sets the local Login instance for clients' use.
func (c *Crawler) Login(login *api.Login) {
	c.configuration.Login = login
}

// Groups instructs the crawler to crawl groups in a manner consistent with the
// current configuration.
func (c *Crawler) Groups(options *CrawlOptions) error {
	if !strings.Contains(c.configuration.OutputFile, ".") {
		if c.configuration.ExportFormat == "json" {
			c.configuration.OutputFile += ".json"
		} else if c.configuration.ExportFormat == "csv" {
			c.configuration.OutputFile += ".csv"
		}
	}

	file, err := os.OpenFile(c.configuration.OutputFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		os.FileMode(0644))
	if err != nil {
		return err
	}

	writer, err := NewGroupWriter(file, c.configuration.ExportFormat)
	defer writer.Close()
	if err != nil {
		return err
	}

	client, err := api.NewGabClient(c.configuration.Login)
	if err != nil {
		return err
	}

	wg := sync.WaitGroup{}
	resc := make(chan *ct.CrawlResult)
	done := make(chan struct{})

	go func() {
		if options.Stop != 0 {
			client.GroupRangeC(resc, done, options.Start, options.Stop)
		} else {
			client.GroupsUntilErrorC(resc, done, options.Start)
		}
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		done <- struct{}{}
	}()

	wg.Add(1)
	go func() {
		for {
			select {
			case result := <-resc:
				group, _ := result.Result.(*ct.Group)
				if group == nil {
					break
				}
				fmt.Println("fetching group ID:", group.ID)
				if err := writer.Write(group); err != nil {
					wg.Done()
					return
				}
			case <-done:
				wg.Done()
				return
			}
		}
	}()

	wg.Wait()
	return nil
}

// TestLogin creates a new client, which attempts to log in with the credentials
// configured for this crawler, and then returns an error on failure.
func (c *Crawler) TestLogin() error {
	client, err := api.NewGabClient(c.configuration.Login)
	if err != nil {
		return err
	}

	c.configuration.Login.Token = client.Token
	return nil
}

// Token returns the current authorization token if available.
func (c *Crawler) Token() string {
	return c.configuration.Login.Token
}
