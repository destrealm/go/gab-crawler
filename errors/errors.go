package errors

import (
	"gitlab.com/destrealm/go/errors"
)

// ErrHTTPStatus is returned whenever the client has determined that the code is
// not acceptable for the current request or client state.
var ErrHTTPStatus = errors.NewError("HTTP status code returned that was not acceptable")

// ErrInvalidFormatRequested is returned if an invalid format was requested for
// the encoder. Presently, only JSON and CSV output is supported, and any other
// type will result in this error.
var ErrInvalidFormatRequested = errors.NewError("invalid format requested")

// ErrInvalidStateFile is returned by the state handler during load and parse if
// no state file was found or the state file itself was corrupted.
var ErrInvalidStateFile = errors.NewError("no state file or invalid file was found")

// ErrLoginFailed is returned if an attempt to login via the Gab user
// authorization endpoint fails.
var ErrLoginFailed = errors.NewError("login failed")

// ErrNonsense is a mildly humorous error returned when a method is requested
// with nonsense arguments.
var ErrNonsense = errors.NewError("nonsense")

// ErrParsingResponse is returned whenever the client did not understand or
// could not parse the response from the server. This could be due to malformed
// HTML or a response that otherwise does not make sense within the current
// context.
var ErrParsingResponse = errors.NewError("could not parse response from server")

// ErrParsingURL is returned if a failure occurs while attempting to parse a
// URL. This is typically returned by the HTTPClient's mode of operations but
// may appear elsewhere.
var ErrParsingURL = errors.NewError("could not parse URL")

// ErrReadingBody is returned in circumstances where reading the response body
// has failed. In most cases, this should be caught by the response's status
// code, but if a status code of 200 is received along with an empty body, this
// error will be returned.
var ErrReadingBody = errors.NewError("could not read response body")

// ErrRequestSetup is returned whenever the client attempts to create a request
// that cannot be handled by net/http.
var ErrRequestSetup = errors.NewError("request setup failed or could not construct a valid request")

// ErrResponseFailure is returned if the http.Response issued by the client
// returned an error.
var ErrResponseFailure = errors.NewError("response failed")

// ErrSavingState is returned if an attempt to save the application state fails.
var ErrSavingState = errors.NewError("could not save state")
