package api_test

import (
	"testing"

	"gitlab.com/destrealm/go/errors"
	"gitlab.com/destrealm/go/gab-crawler/api"
)

// TODO: Implementing a mock interface of Gab's API would be more useful than
// using their live site to run tests against.

func Test_Client(t *testing.T) {
	client, err := api.NewGabClient(&api.Login{
		Email:    config.email,
		Password: config.password,
		Token:    config.token,
	})
	if err != nil {
		t.Error("error creating client:", err)
		t.Error("possible drill down:", errors.Guarantee(err).OriginalError())
	}

	group, err := client.GetGroup(config.groupID)
	if err != nil {
		t.Error("encountered error loading group:", err)
		t.Error("error also encountered:", errors.Guarantee(err).OriginalError())
		return
	}

	if int(group.ID) != config.groupID {
		t.Errorf(`unexpected group ID %d for %d`, group.ID, config.groupID)
	}
}
