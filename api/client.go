package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/destrealm/go/errors"
	. "gitlab.com/destrealm/go/gab-crawler/errors"
	ct "gitlab.com/destrealm/go/gab-crawler/types"
)

type State int

const (
	StateLoggedIn State = iota
)

type GabClient struct {
	Token     string
	Client    *HTTPClient
	State     State
	BaseURL   string
	UserAgent string

	Username  string
	DerivedID int

	MaxErrors int
}

func NewGabClient(login *Login) (*GabClient, error) {
	client := &GabClient{
		Client:    NewHTTPClient("https://gab.com/"),
		BaseURL:   "https://gab.com/",
		UserAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0",
		MaxErrors: 10,
	}

	if login.Token == "" {
		if err := client.Login(login); err != nil {
			return nil, err
		}
	} else {
		client.Token = login.Token
		client.Client.Headers.Add("Authorization",
			fmt.Sprintf("Bearer %s", login.Token))
	}

	return client, nil
}

// GetGroup specified by the `id`.
func (c *GabClient) GetGroup(id int) (*ct.Group, error) {
	var group *ct.Group

	request, err := c.Client.Get(fmt.Sprintf("/api/v1/groups/%d", id))
	if err != nil {
		return nil, err
	}
	response, err := request.Commit()
	if err != nil {
		return nil, err
	}
	if response.StatusCode != 200 {
		return nil, errors.NewErrorWithCode("non-200 status", response.StatusCode)
	}

	b, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, ErrReadingBody.Do(err)
	}

	err = json.Unmarshal(b, &group)
	if err != nil {
		return nil, ErrParsingResponse.Do(err)
	}

	return group, nil
}

// GetGroupPosts retrieves the top 20 most recent posts from the specified
// group. At present, very little introspection is done on these posts and only
// the post ID and creation time are retrieved. No user information is saved as
// we're only currently interested in approximating the group's recent activity.
func (c *GabClient) GetGroupPosts(id int) ([]*ct.GroupPost, error) {
	var posts []*ct.GroupPost

	request, err := c.Client.Get(
		fmt.Sprintf("/api/v1/timelines/group/%d", id))
	if err != nil {
		return nil, err
	}
	response, err := request.Commit()
	if err != nil {
		return nil, err
	}
	if response.StatusCode != 200 {
		return nil, errors.NewErrorWithCode("non-200 status", response.StatusCode)
	}

	b, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, ErrReadingBody.Do(err)
	}

	err = json.Unmarshal(b, &posts)
	if err != nil {
		return nil, ErrParsingResponse.Do(err)
	}

	return posts, nil
}

// GroupRange returns all groups between `start` and `stop`, inclusive. If an
// error is encountered that is not of type ErrHTTPStatus, this will bail and
// return the error. Otherwise, this method will continue collecting groups
// until the maximum boundary is reached. Returns ErrNonsense if start > stop.
func (c *GabClient) GroupRange(start, stop int) ([]*ct.Group, error) {
	if start > stop {
		return make([]*ct.Group, 0), ErrNonsense.Do(errors.New("start value should not be greater than stop"))
	}

	groups := make([]*ct.Group, 0, stop-start)

	for i := start; i <= stop; i++ {
		group, err := c.GetGroup(i)
		if err != nil {
			if !errors.Guarantee(err).Is(ErrHTTPStatus) {
				return make([]*ct.Group, 0), err
			}
		}
		groups[len(groups)] = group
	}

	return groups, nil
}

// GroupRangeC returns all groups between `start` and `stop`, inclusive. If an
// error is encountered that is not of type ErrHTTPStatus, this will bail and
// return the error. Otherwise, this method will continue collecting groups
// until the maximum boundary is reached. Returns ErrNonsense if start > stop.
func (c *GabClient) GroupRangeC(resc chan<- *ct.CrawlResult, done chan<- struct{},
	start, stop int) {
	for i := start; i <= stop; i++ {
		var result *ct.CrawlResult
		var e errors.ErrorTracer

		group, err := c.GetGroup(i)
		if err != nil {
			resc <- &ct.CrawlResult{
				Result: nil,
				Error:  errors.GuaranteeOrNil(err),
			}
			continue
		}

		posts, err := c.GetGroupPosts(i)
		if err != nil {
			resc <- &ct.CrawlResult{
				Result: nil,
				Error:  errors.GuaranteeOrNil(err),
			}
			continue
		}

		group.Posts = posts

		result = &ct.CrawlResult{
			Result: group,
			Error:  e,
		}
		resc <- result
	}
	done <- struct{}{}
}

// GroupsUntilError collects group information until MaxErrorsOnFetch. Returns a
// slice of ct.Group, if successful (otherwise this slice is empty), the last
// successful ID (or 0 if a failure occurred, and the last error encountered
// during scan. Be aware that, while most calls to this function should return
// an error on failure (typically 404, but the value of the error will need some
// introspection to validate), the ONLY way to guarantee that you've reached the
// end of the groups list is to test the length of the returned group slice.
func (c *GabClient) GroupsUntilError(start int) ([]*ct.Group, int, error) {
	var errLast error
	var group *ct.Group
	groups := make([]*ct.Group, 0)
	errors := 0
	id := start
	lastSuccess := 0

	for {
		if errors >= ct.MaxErrorsOnFetch {
			return groups, lastSuccess, errLast
		}

		group, errLast = c.GetGroup(id)
		if errLast != nil {
			errors++
		} else {
			errors = 0
			lastSuccess = id
		}

		groups = append(groups, group)
		id++
	}

	return groups, lastSuccess, nil
}

// GroupsUntilErrorC collects group information until MaxErrorsOnFetch. Returns a
// slice of ct.Group, if successful (otherwise this slice is empty), the last
// successful ID (or 0 if a failure occurred, and the last error encountered
// during scan. Be aware that, while most calls to this function should return
// an error on failure (typically 404, but the value of the error will need some
// introspection to validate), the ONLY way to guarantee that you've reached the
// end of the groups list is to test the length of the returned group slice.
func (c *GabClient) GroupsUntilErrorC(resc chan<- *ct.CrawlResult,
	done chan<- struct{}, start int) {
	var errLast error
	var group *ct.Group
	errc := 0
	id := start

	for {
		var result *ct.CrawlResult

		group, errLast = c.GetGroup(id)
		result = &ct.CrawlResult{
			Result: group,
			Error:  errors.GuaranteeOrNil(errLast),
		}
		if errLast != nil {
			if errors.Guarantee(errLast).Code() != 404 {
				// Gab now throttles connections, so we attempt to play nice by
				// sleeping for a while between 429 Throttled responses.
				if errors.Guarantee(errLast).Code() == 429 {
					fmt.Println("throttled... sleeping for a minute.")
					time.Sleep(60 * time.Second)
				} else {
					fmt.Printf("HTTP %d received; retrying\n",
						errors.Guarantee(errLast).Code())
				}
				continue
			} else {
				id++
				errc++
			}
		} else {
			errc = 0

			posts, err := c.GetGroupPosts(id)
			if err == nil {
				group.Posts = posts
				result.Result = group
			}

			resc <- result
			id++
		}

		if errc >= ct.MaxErrorsOnFetch {
			resc <- result
			done <- struct{}{}
			return
		}
	}
}

// Login using the configured email/password combination, process the response,
// and then extract the bearer token. This may be bypassed by setting a valid
// token directly.
func (c *GabClient) Login(login *Login) error {
	var token struct {
		Meta struct {
			Token string `json:"access_token"`
			User  string `json:"username"`
			ID    string `json:"me"`
		} `json:"meta"`
	}

	request, err := c.Client.Get("/auth/sign_in")
	if err != nil {
		return err
	}
	response, err := request.Commit()
	if err != nil {
		return err
	}
	if response.StatusCode != 200 {
		return ErrHTTPStatus.Do(errors.Errorf("%v", response.StatusCode))
	}

	q, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		return ErrParsingResponse.Do(err)
	}

	t, ok := q.Find("form#new_user input[name=authenticity_token]").First().Attr("value")
	if !ok {
		return ErrParsingResponse.Do(errors.New("could not fetch authenticity_token"))
	}

	request, err = c.Client.Post("/auth/sign_in")
	if err != nil {
		return err
	}
	request.Form.Add("user[email]", login.Email)
	request.Form.Add("user[password]", login.Password)
	request.Form.Add("button", "")
	request.Form.Add("utf8", "✓")
	request.Form.Add("authenticity_token", t)

	response, err = request.Commit()
	if err != nil {
		return err
	}
	if response.StatusCode != 200 {
		return ErrHTTPStatus.Do(errors.Errorf("%v", response.StatusCode))
	}

	q, err = goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		return ErrParsingResponse.Do(err)
	}

	t = q.Find(".alert.flash-message").First().Text()
	if t == "Invalid Email or password." {
		return ErrLoginFailed
	}

	// Successful logins appear to set the location header for redirection. Use
	// that instead?
	t = q.Find("script#initial-state").Contents().Text()
	err = json.Unmarshal([]byte(t), &token)
	if err != nil {
		return ErrParsingResponse.Do(err)
	}

	c.Token = token.Meta.Token
	c.Username = token.Meta.User
	//c.DerivedID = token.Meta.ID

	// Add authorization header to client.
	c.Client.Headers.Add("Authorization", fmt.Sprintf("Bearer %s", c.Token))

	return nil
}
