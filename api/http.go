package api

import (
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	. "gitlab.com/destrealm/go/gab-crawler/errors"
	"golang.org/x/net/publicsuffix"
)

const UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0"

type HTTPClient struct {
	BaseURL        string
	Headers        http.Header
	SessionCookies *cookiejar.Jar
	client         *http.Client
}

func NewHTTPClient(baseURL string) *HTTPClient {
	jar, _ := cookiejar.New(&cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	})

	client := &HTTPClient{
		BaseURL:        baseURL,
		Headers:        http.Header{},
		SessionCookies: jar,
	}

	client.client = &http.Client{
		Jar: client.SessionCookies,
	}
	client.Headers.Add("User-Agent", UserAgent)

	return client
}

func (c *HTTPClient) url() (*url.URL, error) {
	if c.BaseURL != "" {
		return url.Parse(c.BaseURL)
	}
	return url.Parse("https://gab.com/")
}

func (c *HTTPClient) Get(path string) (*HTTPClientRequest, error) {
	u, err := c.url()
	if err != nil {
		return nil, ErrParsingURL.Do(err)
	}

	u.Path = path
	return &HTTPClientRequest{
		method:  "GET",
		url:     u,
		client:  c.client,
		Headers: c.Headers,
	}, nil
}

func (c *HTTPClient) Post(path string) (*HTTPClientRequest, error) {
	u, err := c.url()
	if err != nil {
		return nil, ErrParsingURL.Do(err)
	}

	u.Path = path
	return &HTTPClientRequest{
		method:  "POST",
		url:     u,
		client:  c.client,
		Form:    url.Values{},
		Headers: c.Headers,
	}, nil
}

type HTTPClientRequest struct {
	method  string
	url     *url.URL
	client  *http.Client
	Query   url.Values
	Form    url.Values
	Headers http.Header
	Request *http.Request
}

func (cr *HTTPClientRequest) Commit() (*clientResponse, error) {
	if cr.Form != nil {
		request, err := http.NewRequest(cr.method, cr.url.String(),
			strings.NewReader(cr.Form.Encode()))
		if err != nil {
			return nil, ErrRequestSetup.Do(err)
		}
		cr.Request = request
	} else {
		request, err := http.NewRequest(cr.method, cr.url.String(), nil)
		if err != nil {
			return nil, ErrRequestSetup.Do(err)
		}
		cr.Request = request
	}

	for header, values := range cr.Headers {
		for _, value := range values {
			cr.Request.Header.Add(header, value)
		}
	}

	response, err := cr.client.Do(cr.Request)
	if err != nil {
		return nil, ErrResponseFailure.Do(err)
	}

	return &clientResponse{
		StatusCode: response.StatusCode,
		Body:       response.Body,
		Response:   response,
	}, nil
}

type clientResponse struct {
	StatusCode int
	Body       io.Reader
	Response   *http.Response
}
