package api

type Login struct {
	Email    string
	Password string
	Token    string
}

func (l *Login) IsEmpty() bool {
	return l.Email == "" && l.Password == "" && l.Token == ""
}

type Configuration struct {
	// Login credentials.
	Login *Login

	// Crawler configuration.

	// CredentialsFile indicates the path to a YAML-formatted file containing
	// user credentials. At present, only the authorization token is supported.
	// Default: "credentials.yaml."
	CredentialsFile string

	// ExportFormat may be one of "json" (default) or "csv" for outputing
	// crawled data. JSON-formatted files will always contain data encapsulated
	// in a JSON array. CSV-formatted files will always start with column names.
	ExportFormat string

	// DoNotSaveState disables crawler state saving. Presently, only the
	// authentication token is saved.
	DoNotSaveState bool

	// OutputFile determines the path and file name to which crawled data should
	// be stored. If OutputFile exists, it will be renamed with a timestamp
	// suffix.
	OutputFile string

	// StateFile is the location of the crawler state previously saved if
	// DoNotSaveState is false. Default: ".state.yaml."
	StateFile string

	// Prompter is a function that requests information from the user, if
	// necessary, and returns a *Login struct on success. Depending on the front
	// end, this may request information via the terminal or via a dialog. This
	// function is not used if our local Login instance is populated.
	Prompter func() (*Login, error)
}
