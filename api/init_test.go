package api_test

import (
	"fmt"
	"os"
	"strconv"
	"testing"

	"github.com/joho/godotenv"
)

type testConfig struct {
	email    string
	password string
	token    string
	groupID  int
}

var config *testConfig

func TestMain(m *testing.M) {
	if err := godotenv.Load("../.env"); err != nil {
		fmt.Println("failed to load .env; continuing with environment variables only")
	}

	groupID, err := strconv.Atoi(os.Getenv("GAB_CRAWLER_GROUPID"))
	if err != nil {
		groupID = -1
	}

	config = &testConfig{
		email:    os.Getenv("GAB_CRAWLER_EMAIL"),
		password: os.Getenv("GAB_CRAWLER_PASSWORD"),
		token:    os.Getenv("GAB_CRAWLER_TOKEN"),
		groupID:  groupID,
	}

	os.Exit(m.Run())
}
